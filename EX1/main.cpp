#include "threads.h"
#include <iostream>


using std::cout;
using std::endl;
using std::cin;


int main()
{
	int begin = 0;
	int end = 0;
	std::string filePath;
	int amountOfThreads;
	//declaring var
	cout << "Enter begin: ";
	cin >> begin;
	cout << "Enter end: ";
	cin >> end;
	cout << "Enter file path: ";
	cin >> filePath;
	cout << "Enter amount of threads you want to be created: ";
	cin >> amountOfThreads;
	//getting all info needed from user
	callWritePrimesMultipleThreads(begin, end, filePath, amountOfThreads);
	return 0;
}