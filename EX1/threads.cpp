#include <iostream>
#include <thread>
#include <iostream>
#include <fstream>
#include "threads.h"
#include <mutex>


using std::cout;
using std::endl;
using std::ofstream;

std::mutex m;


/*
	Function gets a range of numbers and a refrence to a file and writes all the prime numbers in the range to the file
	Input:
		begin, end, file
	Output:
		None
*/
void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int i = begin;
	int j = 0;
	bool flag = true;
	//declaring vars
	if (begin < 2)
	{
		begin = 3;
	}
	//negative numbers / 1 / 2 cant be prime numbers
	for (i = begin; i < end; i++)
	{
		//if a number can be divided by 2, it can't be a prime number
		if (i % 2 != 0)
		{
			for (j = 2; j < i / 2 && flag; j++)
			{
				if (i % j == 0)
				{
					flag = false;
				}
			}
			if (flag)
			{
				m.lock();
				file << i << "\n";
				m.unlock();
			}
			flag = true;
			//resetting flags
		}
	}
}


/*
	Function gets a range of numbers, divides it to N small ranges, creates N threads and calling writePrimeToFile from each thread
	Input:
		begin, end, filepath, N
	Output:
		None
*/
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	int i = 0;
	ofstream myFile;
	int count = (end - begin) / N;
	std::thread *threads = new std::thread[N];
	//declaring vars
	myFile.open(filePath);
	if (myFile.is_open())
	{
		for (i = 1; i <= N; i++)
		{
			if (i == N)
			{
				threads[N - 1] = std::thread(writePrimesToFile, begin + (count * (i - 1)), end, ref(myFile));
			}
			else
			{
				threads[i - 1] = std::thread(writePrimesToFile, begin + (count * (i - 1)), begin + (count * i), ref(myFile));
			}
		}
		for (i = 0; i < N; i++)
		{
			threads[i].join();
		}
	}
	else
	{
		cout << "Error opening file" << endl;
	}
}