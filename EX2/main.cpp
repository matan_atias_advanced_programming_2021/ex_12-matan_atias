#include "MessagesSender.h"
#include <iostream>
#include <string>
#include <thread>


using std::cout;
using std::endl;
using std::cin;


#define signinOption 1
#define signoutOption 2
#define connctedUsersOption 3


int main()
{
	int choice = 0;
	std::string username;
	MessagesSender* messagesender = new MessagesSender();
	std::queue<std::string> q;
	std::thread readFromData(&MessagesSender::readDataFromFile, messagesender, ref(q));
	std::thread writeToOutput(&MessagesSender::sendDataToUsers, messagesender, ref(q));
	//declaring vars
	while (true)
	{
		while (choice < 1 || choice > 4)
		{
			messagesender->printMenu();
			cout << "Input: ";
			cin >> choice;
			if (choice < 1 || choice > 4)
			{
				cout << "Invalid choice!" << endl;
				system("PAUSE");
				system("CLS");
			}
		}
		system("CLS");
		switch (choice)
		{
			case signinOption:
				cout << "Enter username: ";
				cin >> username;
				messagesender->signin(username);
				break;
			case signoutOption:
				cout << "Enter username: ";
				cin >> username;
				messagesender->signout(username);
				break;
			case connctedUsersOption:
				messagesender->connectedUsers();
				break;
			default:
				_exit(1);
		}
		system("PAUSE");
		system("CLS");
		choice = 0;
	}
	return 0;
}