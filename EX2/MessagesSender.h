#pragma once

#include <windows.h>
#include <iostream>
#include <string>
#include <set>
#include <fstream>
#include <thread>
#include <queue>
#include <mutex>


class MessagesSender
{
	public:
		MessagesSender();
		~MessagesSender();
		void printMenu();
		void signin(std::string username);
		void signout(std::string username);
		void connectedUsers();
		void readDataFromFile(std::queue<std::string>& q);
		void sendDataToUsers(std::queue<std::string>& q);
	private:
		std::set<std::string> _connectedUsers;
};

