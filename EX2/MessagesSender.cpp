#include "MessagesSender.h"


using std::cout;
using std::endl;
using std::cin;

std::condition_variable cv;
std::mutex m;
std::mutex unique;


/*
	Constructor
	Input:
		None
	Output:
		None
*/
MessagesSender::MessagesSender()
{

}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
MessagesSender::~MessagesSender()
{

}


/*
	Function prints meun
	Input:
		None
	Output:
		None
*/
void MessagesSender::printMenu()
{
	cout << "1. Signin" << endl;
	cout << "2. Signout" << endl;
	cout << "3. Connected Users" << endl;
	cout << "4. exit" << endl;
}


/*
	Function gets a username and signs a user in if he isn't signed in already
	Input:	
		username
	Output:
		None
*/
void MessagesSender::signin(std::string username)
{
	m.lock();
	if (this->_connectedUsers.find(username) != this->_connectedUsers.end())
	{
		cout << "User is already logged in!" << endl;
	}
	else
	{
		this->_connectedUsers.insert(username);
		cout << "User signed in successfully!" << endl;
	}
	m.unlock();
}


/*
	Function gets a username and signs a user out if he is signed in
	Input:
		username
	Output:
		None
*/
void MessagesSender::signout(std::string username)
{
	m.lock();
	if (this->_connectedUsers.find(username) != this->_connectedUsers.end())
	{
		this->_connectedUsers.erase(username);
		cout << "User signed out successfully!" << endl;
	}
	else
	{
		cout << "User isn't logged in!" << endl;
	}
	m.unlock();
}


/*
	Function prints all the connected users
	Input:
		None
	Output:
		None
*/
void MessagesSender::connectedUsers()
{
	m.lock();
	for (std::set<std::string>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); ++it)
	{
		cout << std::string(*it) << endl;
	}
	m.unlock();
}


/*
	Function reads from data.txt and
	Input:
		file fstream
	Output:
		None
*/
void MessagesSender::readDataFromFile(std::queue<std::string>& q)
{
	std::string line;
	std::ifstream dataFileRead;
	std::ofstream dataFileWrite;
	//declaring vars
	while (true)
	{
		dataFileRead.open("data.txt");
		if (dataFileRead.is_open())
		{
			std::unique_lock<std::mutex> lck(unique);
			while (std::getline(dataFileRead, line))
			{
				q.push(line);
			}
			dataFileRead.close();
			//opening "data.txt" in read mode, saving the contents of it and closing it
			dataFileWrite.open("data.txt");
			dataFileWrite.clear();
			dataFileWrite.close();
			//opening "data.txt" in write mode, clearing the contents of it and closing it
			lck.unlock();
			cv.notify_one();
		}
		else
		{
			cout << "ERROR READING DATA.TXT!" << endl;
			system("PAUSE");
			system("CLS");
		}
		Sleep(10000);
	}
}


/*
	Function sends messages to users
	Input:
		queue
	Output:
		None
*/
void MessagesSender::sendDataToUsers(std::queue<std::string>& q)
{
	std::ofstream outputFile;
	//declaring vars
	outputFile.open("output.txt");
	if (outputFile.is_open())
	{
		while (true)
		{
			std::unique_lock<std::mutex> lck(unique);
			cv.wait(lck);
			while (!q.empty())
			{
				m.lock();
				for (std::set<std::string>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); ++it)
				{
					outputFile << std::string(*it) << ": " << q.front() << endl;
				}
				q.pop();
				m.unlock();
			}
			lck.unlock();
		}
	}
	else
	{
		cout << "Error opening file!" << endl;
		system("PAUSE");
		system("CLS");
	}
	outputFile.close();
}